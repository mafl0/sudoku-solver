/**
 * Test driver for the sudoku solver
 * Copyright (C) 2017 Martijn Fleuren
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <pthread.h>
#include <string.h>
#include <errno.h>

#include "sudoku.h"
#include "parser.h"
#include "threaded.h"
#include "options.h"
#include "debug.h"

int main(int argc, char **argv) {

  ProgramOptionsT * options = getProgramOptions(argc, argv);

  time_t before, after;
  double seconds = 0;

  // Load all sudoku puzzles into ram. The first line of the test file contains
  // the number of test cases @N@. The next N lines contain 81 digits each.
  FILE * f = fopen(options->filepath, "r");

  // Read N
  size_t numTestCases = pInt(f);

  printf("Reading %lu sudokus from %s.\n", numTestCases, options->filepath);

  // Read N lines of 81 digits, then close f
  board_t ** sudokus = (board_t **) calloc(numTestCases, sizeof(board_t *));
  sudokus = parseBoards(f, numTestCases);

  fclose(f);

  // Solve the sudokus and time the time it takes

  printf("Done reading all examples into memory. Starting to solve all of them now, output omitted for speed.\n");

  _tupleT  ** ts  = (_tupleT **) calloc(options->threads, sizeof(_tupleT *));

  if (errno == ENOMEM) {
    perror("Error: Out of memory.");
    abort();
  }

  time(&before);

  pthread_t * workers = forkWorkers(options->threads, numTestCases, sudokus, ts);
  waitForWorkers(options->threads, workers);

  time(&after);

  seconds = difftime(after, before);

  printf("Took %.2f seconds to process %lu sudokus. Average %.2f/s\n"
      , seconds, numTestCases, (double) numTestCases / seconds);

  freeBoards(sudokus, numTestCases);

  for (size_t i = 0; i < options->threads; ++i)
    free(ts[i]);
  free(ts);
  
  free(options);

  return EXIT_SUCCESS;
}
