/**
 * Solver library that implements DFS for solving sudokus
 * Copyright (C) 2017 Martijn Fleuren
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *
 * Sudoku solver
 *
 * === Description ===
 * This program will solve a sudoku by performing DFS. It works its way from the
 * top left to the bottom right. Candidate numbers are computed at each empty
 * cell[1] and are then guessed in ascending fashion.
 *
 * === Computation of the candidates ===
 * Candidates 1-9 are held in a 16-bit unsigned number, where the 0 bit is
 * always unset (0 means that the cell is empty), and 1-9 are set when they are
 * ecountered in either the row, column or block. The negation of the number
 * then contains all the numbers that have not yet been seen with the exclusion
 * of the 0th bit. It is excluded because guessing will start at i = 1.
 *
 * Analysis:
 * If n is the side length of the sudoku then finding all hints takes 3n (one
 * for each of row, column and block) so that takes O(n) time, storage is 1
 * integer per cell per recursion so O(1) space. In the worst case there are in
 * the order of n^2 cells to run the above process on. The time complexity
 * therefore is O(n^3) and the space complexity O(n^2). Since O(n^3) > O(n^2)
 * for all n i conclude that this algorithm will take O(n^3) time.
 *
 * === Notes ===
 * [1] If the cell is non empty (i.e. the cell contains a hint) the program
 *     simply moves to the next cell.
 * [2] The solution that is found is the smallest. The value of a sudoku is the
 *     the value of the natural number one would get when laying a solution out
 *     from top left to bottom right in decimal.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

#include "sudoku.h"
#include "parser.h"

// Switch board validation when a solution has been found
#define validate_board 0

/**
 * Pretty printing of a board
 */

static void printRow(board_t * board, const size_t pos) {
  
  if (board == NULL)
    return;

  size_t y = pos - (pos % 9);

  for (size_t x = 0; x < 9; ++x) {
    if (board[y+x] > (board_t) 0)
      printf("%u ", (unsigned int) board[y+x]);
    else
      printf("  ");

    if (x == 2 || x == 5)
      printf("| ");
  }
  (void) putchar('\n');

  return;
}

void printBoard(board_t * board, const LineType mode) {
  
  if (board == NULL)
    return;

  size_t inc = (size_t) (mode == boardT ? 9 : 1);

  for (size_t pos = 0; pos < SDK_BOARD_SIZE; pos += inc) {
    if (mode == boardT) {
      printRow(board, pos);
      if (pos == 18 || pos == 45) printf("------+-------+------\n");
    } else {
      printf("%u", (unsigned int) board[pos]);
    }
  }

  if (mode == lineT)
    (void) putchar('\n');
}

/**
  * Get the numbers belonging to a row that `pos` belongs to. The base y
  * position is computed to do so, which is basically the first multiple of 9
  * below pos.
  *
  * The next 9 numbers from that base y position are the row numbers. Note that
  * only numbers >0 are stored because 0 symbolises the empty cell.
  */
static uint16_t getRow(board_t * board, const unsigned int pos) {
  
  if (board == NULL)
    return 0;

  uint16_t k = 0;
  unsigned int y = pos - (pos % 9);

  for (size_t i = 0; i < 9; ++i)
    k |= 1 << board[y+i];
  return k;
}

static uint16_t getColumn(board_t * board, const unsigned int pos) {
  // Get column numbers between two bounds
  
  if (board == NULL)
    return 0;

  uint16_t k = 0;
  unsigned int x = pos % 9;

  for (size_t i = 0 ; i < SDK_BOARD_SIZE; i += 9)
    k |= 1 << board[i+x];

  return k;
}

static uint16_t getBlock(board_t * board, const unsigned int pos) {
  /**
   * Get the contents of a block.
   *
   *   Get start value -> Generate sequence for positions of the cells inside
   * the block -> use the positions to get the block contents.
   */
  
  if (board == NULL)
    return 0;

  unsigned int
  // x and y positions in the usual 9x9 sudoku grid
      x    =  pos      % 9
    , y    = (pos - x) / 9


  // Startvalue and buffer for the sequence generation
    , startVal
    ;

  // loop counter and block number buffer (k)
  uint16_t k = 0;

  // Determine the value to start the sequence generator on. Note that the order
  // in which these are evaluated matters because the less strict checks are
  // excluded by the checks that were performed earlier.
  if (x < 3 && y < 3) {
    startVal = 0;
  } else if (x < 3 && y < 6) {
    startVal = 27;
  } else if (x < 3) {
    startVal = 54;
  } else if (x < 6 && y < 3) {
    startVal = 3;
  } else if (x < 6 && y < 6) {
    startVal = 30;
  } else if (x < 6) {
    startVal = 57;
  } else if (y < 3) {
    startVal = 6;
  } else if (y < 6) {
    startVal = 33;
  } else {
    startVal = 60;
  }

  /**
   * One dimensional coordinate buffer is filled with the sequence. For example
   * if x < 6 and y < 3 then the sequence is started at 3.
   *
   *        add 6(+1) when i = 1+1(+1) = 3      until i > 20
   *              |                             |
   *              v                             v
   *   +1   +1   +7    +1    +1    +7    +1    +1
   * 3    4    5    12    13    14  ^ 21    22    23
   *                                |
   *                                add 6(+1) again when i = 1+1+6(+1)+1+1 = 12
   *
   * in coordinate (x, y) notation these would correspond to
   * (3, 0), (4, 0), (5, 0), (3, 1), (4, 1), (5, 1), (3, 2), (4, 2), (5, 2)
   * where (0, 0) is the top left and (8,8) is the bottom right.
   */

  // sequence generation
  for (size_t i = 0; i <= 20; ++i) {
    if (i == 3 || i == 12) {
      i += 6; // not 7 because the 1 is coming from the statement above.
    }
    k |= 1 << board[startVal + i];
  }

  return k;
}

// Array of functions to find row, column and block numbers.
static uint16_t (*functions[])(board_t *,unsigned int) = {getRow, getColumn, getBlock};

/**
  * Computes the candidate numbers for cell `pos' in `board'. Does so by
  * linearly searching through row, block and column for numbers that are not
  * candidates. The remaining numbers are candidates.
  *
  * Notes:
  *  o Assume that board[pos] is an empty cell (i.e. has value 0)
  *  o For the returned number `c' in binary notation, c[i] == 1 iff i is a
  *    candidate number for board[pos]
  */
static uint16_t possibilities(board_t * board, const unsigned int pos) {
  
  if (board == NULL)
    return 0;

  // `candidates' will be the result and is inverted at the end. initialising to
  // 0 ensures that candidates[0] == 0 in the end.
  uint16_t candidates = 1;

  // Linearly search for numbers that are not candidates.
  for (size_t i = 0; i < 3; ++i)
    candidates |= (*functions[i])(board, pos);

  // Invert such that all bits for numbers that have not been found are 1
  return ~candidates;
}

unsigned int validBoard(board_t * board) {
  
  if (board == NULL)
    return 0;

  unsigned int i, j;

  // test that all values on the board are between 1 and 9
  // Note: 0x03fe = 0000 0011 1111 1110, the integer value where positions 1
  // through 9 are 1.
  for (i = 0; i < SDK_BOARD_SIZE; ++i) {
    for (j = 0; j < 3; ++j) {
      if ((*functions[j])(board, i) != 0x03fe)
        return 0;
    }
  }

  return 1;
}

/**
  * Solve the board starting from `pos'.
  *
  * If pos is 81 then the board is done, optionally the board can be validated
  * with the validBoard() function
  *
  * If board[pos] > 0 then the cell contains a hint and there is no guessing
  * required.
  *
  * Otherwise all the candidates for this cell have to be found:
  *   If there are no candidates then the search space is empty, and since pos
  *   != 81 and pos == 0, the board is unfinished and unsolved. return to the
  *   previous branch that can try another candidate and move on from there.
  *
  *   If there are candidates, they are guessed in ascending order recursing
  *   for each candidate. If a solution is found with the current guess then no
  *   other guesses are tried.
  */
static unsigned int _solve(board_t * board, const unsigned int pos) {

  // If we reach this position, we are done
  if (pos == SDK_BOARD_SIZE) {
#if validate_board == 0
    return 1;
#else
    validBoard(board);
#endif
  }

  // If the current position is a hint, skip it
  if (board[pos] > (board_t) 0) 
    return _solve(board, pos+1);

  uint16_t k = possibilities(board, pos);

  // if (!k) return 0; // not covered?

  board_t orig;
  for (size_t i = 1; i < 10; ++i) {
    if ((k & (1 << i)) > 0) {
      orig = board[pos];
      board[pos] = (board_t) i;
      if (_solve(board, pos+1) > 0)
        return 1;
      board[pos] = (board_t) orig;
    }
  }
  return 0;
}

unsigned int solve(board_t * board) {
  // Helper function for solve, it only fills in the start position to 0
  return _solve(board, 0);
}

