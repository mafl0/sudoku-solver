#ifndef __SUDOKU_H__
#define __SUDOKU_H__

#define SDK_BOARD_SIZE 81

typedef unsigned char board_t;

unsigned int solve(board_t * b);

typedef enum line_type
  { lineT
  , boardT
  } LineType;

void printBoard(board_t * board, LineType mode);

#endif //__SUDOKU_H__
