#ifndef __SUDOKU_OPTIONS_H__
#define __SUDOKU_OPTIONS_H__

typedef struct ProgramOptions {
  char * filepath;
  unsigned int threads;
} ProgramOptionsT;

ProgramOptionsT * getProgramOptions(int argc, char **argv);

#endif //__SUDOKU_OPTIONS_H__
