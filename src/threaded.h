#ifndef __SUDOKU_THREADED__
#define __SUDOKU_THREADED__

typedef struct ThreadedSudoku {
  uint16_t amountToCheck;
  uint8_t threads;
  pthread_t * workers;
  board_t ** sudokus;
} ThreadedSudokuT;

typedef struct _tuple {
  size_t amount;
  size_t start;
  size_t end;
  board_t ** sudokus;
} _tupleT;


void
waitForWorkers(uint8_t threads, pthread_t * workers);

pthread_t *
forkWorkers(size_t threads, size_t amount, board_t ** sudokus, _tupleT ** ts);

ThreadedSudokuT *
setupTester( size_t amountToCheck
           , board_t * sudokus
           , uint8_t threads
           );
#endif //__SUDOKU_THREADED__
