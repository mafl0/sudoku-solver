#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <errno.h>

#include "debug.h"
#include "sudoku.h"
#include "parser.h"

#ifndef NDEBUG
static bool validInput(board_t * b) {
  bool ret = true;

  for (size_t i = 0; i < SDK_BOARD_SIZE; ++i) {
    if (9 < b[i]) { // unsigned int is always > 0
      ret = false;
      break;
    }
  }

  return ret;
}
#endif

// Sentinel for functions that have file pointers as their argument
static void _filePtrNotNull(FILE * h) {
  if (h == NULL) {
    errno = EBADF;
    perror("File descriptor is NULL");
    abort();
  }
}

static char _str[3];
static char * show(char c) {

  switch (c) {
    case '\n':
      _str[0] = '\\';
      _str[1] = 'n';
      _str[2] = '\0';
      break;

    case '\r':
      _str[0] = '\\';
      _str[1] = 'r';
      _str[2] = '\0';
      break;

    default:
      _str[0] = c;
      _str[1] = '\0';
      break;
  }

  return _str;
}

// read one digit from h
static char pDigit(FILE * h) {

  _filePtrNotNull(h);
 
  char c = fgetc(h);

  if (!('0' <= c && c <= '9')) {
    printf( "No parse: Expected a digit but got \'%s\' at position %ld\n"
          , show(c), ftell(h));
    abort();
  }

  return c;
}

// read @number@ of digits from h
static board_t * pDigits(FILE * h, const size_t number) {

  _filePtrNotNull(h);

  board_t * c = calloc(number, sizeof(board_t));

  if (errno == ENOMEM) {
    perror("Error: Out of memory.");
    abort();
  }

  for (size_t i = 0; i < number; ++i)
    c[i] = pDigit(h);

  return c;
}

// read eol
static void eol(FILE * h) {
  _filePtrNotNull(h);

  char c[2];

  c[0] = fgetc(h);
  c[1] = fgetc(h);

  if (c[0] == '\n') {
    fseek(h, -1, SEEK_CUR);
    return;                          // '\n'   LF
  } else if (c[0] == '\r') {
    if (c[1] != '\n')
      fseek(h, -1, SEEK_CUR);        // '\r'   CR
    return;                          // '\r\n' CRLF
  } else {
    printf("No parse: expected \\n \\r or \\r\\n");
    exit(EXIT_FAILURE);
  }
}

/**
 * Read a string from file handle @h@ and parse a board from it. Assume that the
 * string is correct. The side effect of this function is that the position in
 * @h@ will be advanced to the position after the board that has been parsed.
 *
 * Syntax
 *
 * boards ::= board* eof
 * board  ::= number{81} eol
 * number ::= [0-9]
 * eol    ::= '\n' | '\r' | '\r\n'
 */
board_t * parseBoard(FILE * h) {

  _filePtrNotNull(h);

  board_t * out = (board_t *) pDigits(h, SDK_BOARD_SIZE);
  eol(h);

  for (size_t i = 0; i < SDK_BOARD_SIZE; ++i)
    out[i] -= '0';

  #ifndef NDEBUG 
  if (!validInput(out)) {
    printf("Board invalid\n");
  }
  #endif

  return out;
}

board_t ** parseBoards(FILE * h, const size_t number) {
  board_t ** out = (board_t **) calloc(number, sizeof(board_t *));

  for (size_t i = 0; i < number; ++i)
    out[i] = parseBoard(h);

  return out;
}

void freeBoards(board_t ** boards, const size_t number) {

  if (boards == NULL || number == 0)
    return;

  for (size_t i = 0; i < number; ++i) {
    free(boards[i]);
  }
  free(boards);
}

// Parse a single, unsigned integer on a line
size_t pInt(FILE * h) {
  unsigned int res;
  (void) fscanf(h, "%u", &res);
  eol(h);
  return (size_t) res;
}

