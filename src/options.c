#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <errno.h>
#include <unistd.h>

#include "options.h"

static const char helpMsg[] =
  "Sudoku tester program that solves sudoku puzzles by depth-first search, and can\n"
  "use multiple cores.\n"
  "\n"
  " -f <file>   file with sudokus to solve\n"
  " -t <num>    number of threads to use. 1 if not specified\n"
  " -h          print this help\n";

static inline void printHelp() {
  (void) fwrite(helpMsg, sizeof(helpMsg[0]), sizeof(helpMsg), stderr);
}

ProgramOptionsT * getProgramOptions(int argc, char **argv) {

  if (argc == 0 || argv == NULL) {
    perror("Error while parsing program options. argc = 0 or argv = NULL");
    abort();
  }

  ProgramOptionsT * out = (ProgramOptionsT *) malloc(sizeof(ProgramOptionsT));

  if (out == NULL || errno == ENOMEM) {
    perror("Allocation of ProgramOptionsT out failed.");
    abort();
  }

  out->threads  = 1;  // default value
  out->filepath = "";

  char c;

  while ((c = (char) getopt(argc, argv, "f:t:h")) != (char) -1) {
    switch (c) {
      case 'f': // Filepath
        out->filepath = optarg;
        break;

      case 't': // Number of threads
        (void) sscanf(optarg, "%u", &out->threads);
        break;

      case 'h': // print help
        printHelp();
        exit(0);

      default:
        printf("Invalid option, ignoring. pass -h to see help\n");
        break;
    }
  }

  // This check has to be done according to the GNU getopt specification
  if (optind != argc) {
    printf("You gave more options than have been processed.\n");
  }

  return out;
}
