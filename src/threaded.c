#include <errno.h>
#include <pthread.h>
#include <stdatomic.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "sudoku.h"
#include "threaded.h"
#include "parser.h"

/**
 * A worker will solve all sudokus in the range that it has been assigned. No
 * locking required.
 */
void worker(_tupleT * setup) {

  if (setup == NULL)
    return;

  for (size_t ix = setup->start; ix < setup->end; ++ix)
    solve(setup->sudokus[ix]);
  
  pthread_exit(NULL);
}

/**
 * Forks a number of workers to solve sudokus. Returns an array of size
 * @setup->amounttoCheck@ that contains the thread id's of the workers.
 */
pthread_t * forkWorkers(size_t threads, size_t amount, board_t ** sudokus, _tupleT ** ts) {

  if (sudokus == NULL || ts == NULL) {
    perror("Expected `sudokus' and `ts' variable to be non-NULL.\n");
    abort();
  }

  pthread_t * out = (pthread_t *) calloc(threads, sizeof(pthread_t));

  if (errno == ENOMEM) {
    perror("Error: Out of memory.");
    abort();
  }

  printf("Will fork %lu workers\n", threads);

  // Make the thread explicitly joinable
  pthread_attr_t attr;
  pthread_attr_init(&attr);
  pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

  size_t stepsz = amount / threads;

  // Fork the workers
  for (size_t i = 0; i < threads; ++i) {

    ts[i] = (_tupleT *) malloc(sizeof(_tupleT));

    ts[i]->amount  = amount;
    ts[i]->sudokus = sudokus;

    ts[i]->start   = (i == 0) ? 0 : ts[i-1]->end;
    ts[i]->end     = ts[i]->start + stepsz;

    pthread_create(&out[i], &attr, worker, (void *) ts[i]);
    printf("forked worker #%lu (%lu <= ix < %lu)\n", i, ts[i]->start, ts[i]->end);
  }

  ts[threads - 1]->end = amount - 1;

  // Cleanup the attribute
  pthread_attr_destroy(&attr);

  return out;
}

/**
 * Join an array of threads back into the parent.
 */
void waitForWorkers(uint8_t threads, pthread_t * workers) {

  if (workers == NULL)
    return;

  for (size_t i = 0; i < threads; ++i) {
    pthread_join(workers[i], NULL);
    printf("joined worker #%lu\n", i);
  }
}

