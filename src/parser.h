#ifndef __SUDOKU_PARSER_H__
#define __SUDOKU_PARSER_H__

#ifdef debug
bool validInput(board_t * b);
#endif

board_t *  parseBoard(FILE * h);
board_t ** parseBoards(FILE * h, const size_t amount);
void freeBoards(board_t ** boards, const size_t number);

size_t pInt(FILE * h);

#endif //__SUDOKU_PARSER_H__
